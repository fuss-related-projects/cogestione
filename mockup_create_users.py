from django.contrib.auth.models import User
from cogestione.models import Utente
import random

# Some names (for random generation)
nomi = ['Adriano', 'Agatha', 'Alberto', 'Alessandro', 'Alessia', 'Alice', 'Anchise', 'Andrea', 'Angela', 'Anna', 'Anja', 'Aurora', 'Azzurra', 'Barbara', 'Beatrice', 'Bianca', 'Bryan', 'Camilla', 'Carla', 'Caroline', 'Cecilia', 'Chanel', 'Chiara', 'Christian', 'Claudio', 'Costanza', 'Cristina', 'Daniela', 'Debora', 'Denise', 'Dennis', 'Desiderio', 'Diana', 'Diego', 'Donatella', 'Ed', 'Elena', 'Eleonora', 'Elia', 'Elisa', 'Elizabeth', 'Emma', 'Ettore', 'Evelyn', 'Fabio', 'Federico', 'Franca', 'Francesco', 'Frida', 'Fulvio', 'Gino', 'Giordano', 'Giorgia', 'Giulia', 'Giuseppe', 'Grace', 'Harald', 'Iris', 'Jacopo', 'Kevin', 'Lara', 'Laura', 'Leonardo', 'Lidia', 'Lorenzo', 'Luigi', 'Marco', 'Mario', 'Martina', 'Martino', 'Massimo', 'Matteo', 'Maurizio', 'Mauro', 'Michelangelo', 'Michele', 'Mirella', 'Mohammed', 'Nicole', 'Nina', 'Nunziatina', 'Paola', 'Paolo', 'Paride', 'Piergiorgio', 'Piero', 'Piero', 'Pietro', 'Riccardo', 'Rossana', 'Sandra', 'Sara', 'Silvia', 'Simone', 'Sofia', 'Stefano', 'Teresa', 'Valentina', 'Vincenzo', 'Viola', 'Walther']
cognomi = ['Ferri', 'Moretti', 'Greco', 'Piccolo', 'Brambilla', 'Conte', 'Rizzi', 'Guerra', 'Caputo', 'Di Giovanni', 'Sorrentino', 'Pellegrini', 'Perrone', 'Marini', 'Piazza', 'Ruggiero', 'Guerra', 'Monaco', 'Sala', 'Ferrero', 'Agostini', 'Ruggiero', 'Fontana', 'Brambilla', 'Lombardi', 'Palumbo', 'Mariani', 'Morelli', 'Rizzi', 'Moro', 'Vitali', 'Pagano', 'Testa', 'Rinaldi', 'Cavallo', 'Giordano', 'Villani', 'Pinna', 'Bianco', 'Marini', 'Messina', 'Baldi', 'Marino', 'Longo', 'Galli', 'Leone', 'Rossetti', 'Mele', 'Palmieri', 'Mazza', 'Calabrese', 'Perrone', 'Bernardi', 'Esposito', 'Barbieri', 'Sorrentino', 'Pastore', 'Pellegrini', 'Battaglia', 'Marchetti', 'Castelli', 'Antonelli', 'Longo', 'Ferrante', 'Caputo', 'Olivieri', 'Longo', 'Piazza', 'Volpe', 'Guerra', 'Parisi', 'Fumagalli', 'Costantini', 'Martini', 'Bruno', 'Gatti', 'De Simone', 'Esposito', 'Marchetti', 'Valente', 'Ferrero', 'Giuliani', 'Santoro', 'Benedetti', 'Grassi', 'Piazza', 'Grasso', 'Rossi', 'Piazza', 'Albanese', 'Villani', 'Riva', 'Pastore', 'Ferro', 'Vitale', 'Pozzi', 'Parisi', 'Cattaneo', 'Arena', 'Caputo', 'Fontana']

for i in range(500):
    try:
        nome = random.choice(nomi)
        cognome = random.choice(cognomi)
        uname = "%s%s" % (cognome, nome)
        u = User.objects.create(
            username=uname, last_name=cognome, first_name=nome
        )
        Utente.objects.create(account=u, docente=False, amministratore=False)
        print(i, "...", end="", flush=True)
    except:
        pass
