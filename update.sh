#!/bin/bash

git pull origin master

if [ -e env ] ; then
	. env/bin/activate
fi

python3 manage.py makemigrations
python3 manage.py migrate
