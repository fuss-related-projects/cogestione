#!/usr/bin/python
# -*- coding: utf-8 -*-
# File: views.py
#
# Copyright (C) 2016 Marco Marinello <mmarinello@sezf.it>
# Copyright (C) 2018 Marco Marinello <mmarinello@sezf.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login, logout, models as authmd
from ldap3 import Server, Connection, core
from cogestione.models import Utente

def add_danger(request, message):
    messages.add_message(request, "50", message, extra_tags="danger")
messages.danger = add_danger


def ldap_auth(request, name, pssw, _next):
    try:
        server = Server(settings.LDAP_SERVER)
        dn = "uid=%s,%s" % (name, settings.LDAP_DN)
        conn = Connection(server, dn, pssw, auto_bind=True)
        print(conn, '\n', conn.extend.standard.who_am_i())

        try:
            user = authmd.User.objects.get(username=name)
        except:
            user = authmd.User.objects.create(username=name)
        user.set_password(pssw)

        conn.search(dn, '(&(objectclass=person))', attributes=["gecos"])

        if len(conn.entries) == 1:
            try:
                gecos = conn.entries[0].gecos[0]
                if user.get_full_name() != gecos:
                    user.first_name = gecos.split(" ")[0]
                    user.last_name = " ".join(gecos.split(" ")[1:])
                    messages.success(
                    request,
                    _("Il tuo gecos è stato allineato con quello di LDAP.")
                    )
            except Exception as e:
                print("GECOS update exception:", e)

        user.save()

        try:
            Utente.objects.get(account=user)
        except:
            Utente.objects.create(account=user)

        login(request, user)
        return redirect("main-index")

    except core.exceptions.LDAPBindError as e:
        print(e)
        messages.warning(
            request,
            "%s! %s." % (
                _("Password errata"),
                _("La password che hai immesso è errata")
                )
            )
        return redirect("landing")
    except Exception as e:
        print("LDAP authentication exception:", e)
        messages.danger(
            request,
            "%s. %s." % (
                _("Uscita improvvisa di LDAP"),
                _("L'autenticatore è terminato prima di fornire un token valido. Se l'errore persiste, segnalare il problema all'amministratore")
                )
            )
        return redirect("landing")
