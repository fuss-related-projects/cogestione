# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from cogestione18.ldapOauth import ldap_auth


# Some danger message
def add_danger(request, message):
    messages.add_message(request, "50", message, extra_tags="danger")
messages.danger = add_danger


def landing(request):
    if request.user.is_authenticated:
        return redirect("main-index")
    return render(request, "login/landing.html")


def main(request):
    if not request.user.is_authenticated:
        return redirect("landing")
    return redirect("cogestione:index")


def logon(request):
    if request.POST:
        if "username" in request.POST and "password" in request.POST:
            username = request.POST["username"]
            password = request.POST["password"]
            _n = request.POST.get("next", "main-index")
            user = authenticate(
                request,
                username=username,
                password=password
            )
            if user:
                login(request, user)
                messages.success(
                    request,
                    _("Benvenuto, %s!") % (user.get_full_name() or user.username)
                )
                return redirect(_n)
            else:
                return ldap_auth(request, username, password, _n)
    return redirect("landing")


@login_required
def logoff(request):
    logout(request)
    messages.success(
        request,
        _("Sessione terminata. A presto!")
    )
    return redirect("main-index")
