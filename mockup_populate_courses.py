from cogestione.models import Corso, Utente
import random

pool = Utente.objects.filter(docente=False)

corsi = Corso.objects.all()

for corso in corsi:
    if not corso.studenti.all():
        print("Populating", corso.nome, "...", end="", flush=True)
        for i in range(corso.numero_max_studenti_per_classe):
            add = random.choice(pool)
            while add in corso.studenti.all():
                add = random.choice(pool)
            corso.studenti.add(add)
            print(i, "...", end="", flush=True)
        corso.save()
