from django import template
from cogestione.models import Utente


register = template.Library()

def is_teacher(user):
    try:
        acc = Utente.objects.get(account=user)
        return acc.docente or acc.amministratore
    except:
        return False

register.filter("teacher", is_teacher)
