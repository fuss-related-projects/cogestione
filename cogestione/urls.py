# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""cogestione18 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from . import views

urlpatterns = [
    path('', views.corsi, name='index'),
    path('studente/iscrivi', views.iscrivi, name='studente-iscrivi'),
    path('studente/disiscrivi', views.cancella_iscrizione, name='studente-disiscrivi'),
    path('docente/', views.home_docente, name='docente-home'),
    path('docente/miei_corsi', views.corsi_docente, name='docente-miei-corsi'),
    path('docente/tutti_i_corsi', views.tutti_i_corsi_docente, name='docente-tutti-i-corsi'),
    path('docente/<int:corso>', views.corso_docente, name='docente-corso'),
    path('docente/<int:corso>/ajax', views.studenti_classe_ajax, name='docente-studenti-classe-ajax'),
    path('docente/<int:corso>/firme_ajax', views.firme_classe_ajax, name='docente-firme-classe-ajax'),
    path('docente/<int:corso>/cambio_stato', views.cambio_stato, name='docente-cambio-stato'),
    path('docente/<int:corso>/cancella_stato', views.cancella_stato, name='docente-cancella-stato'),
    path('docente/<int:corso>/cancella_firma', views.cancella_firma, name='docente-cancella-firma'),
    path('docente/<int:corso>/firma', views.esegui_firma, name='docente-firma'),
    path('docente/<int:corso>/firma_selettiva', views.esegui_firma_selettiva, name='docente-firma-selettiva'),
    path('coord/index', views.coord_index, name='coord-home'),
    path('coord/studenti/ajax', views.coord_studenti_ajax, name='coord-studenti-ajax'),
    path('coord/firme/ajax', views.coord_firme_ajax, name='coord-firme-ajax'),
]
