# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def stato_alla_lezione(stato, corso, puo_modificare):
    stcl = {
        "PL": "btn-success",
        "PX": "btn-grey",
        "R": "btn-warning",
        "RB": "btn-warning",
        "AL": "btn-danger"
    }

    return """<a href="#" class="btn btn-block {}" title="{}" onclick="{}">{}</a>""".format(
        stcl[stato.stato] if stato.firma.corso == corso else "btn-primary",
        "%s è stato %s a %s %s° ora (%s)" % (
            stato.studente.account.get_full_name(), stato.stato,
            stato.firma.corso.nome, stato.firma.ora,
            stato.firma.docente.account.get_full_name()
        ),
        "modalestudente('%s', '%s', '%s', '%s', '%s')" %(
            stato.studente.account.get_full_name(), stato.firma.ora, stato.stato,
            stato.id, stato.commenti
        ) if puo_modificare else "",
        stato.stato
    )


def stato_alla_lezione_coord(stato):
    stcl = {
        "PL": "btn-success",
        "PX": "btn-grey",
        "R": "btn-warning",
        "RB": "btn-warning",
        "AL": "btn-danger"
    }

    return """<a href="#" class="btn btn-block {}" title="{}">{}</a>""".format(
        stcl[stato.stato],
        "%s è stato %s a %s %s° ora (%s)" % (
            stato.studente.account.get_full_name(), stato.stato,
            stato.firma.corso.nome, stato.firma.ora,
            stato.firma.docente.account.get_full_name()
        ),
        stato.stato
    )



def checkbox(studente):
    return """<input id='studente' type='checkbox' id_studente='{}' nome_studente='{}'>""".format(
        studente.id, studente.account.get_full_name()
    )


def elimina_firma(firma, utente):
    if firma.docente.account != utente:
        return ""
    return """<a href='#' class='red red-hover fa fa-close fa-2x' onclick="eliminafirma('%s');"></a>""" % firma.id
