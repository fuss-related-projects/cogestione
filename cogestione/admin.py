from django.contrib import admin
from .models import *


class UtenteAdmin(admin.ModelAdmin):
    model = Utente
    search_fields = ["id", "account__username"]
    def get_list_display(self, request):
        return ("account",)

admin.site.register(Utente, UtenteAdmin)


class CorsoAdmin(admin.ModelAdmin):
    model = Corso
    search_fields = ["id", "nome", "descrizione"]
    save_as = True
    def get_list_display(self, request):
        return ("nome", "ora_inizio", "ora_fine")

admin.site.register(Corso, CorsoAdmin)


class PartAdmin(admin.ModelAdmin):
    model = Partecipazione
    search_fields = ["id", "studente__account__username", "corso__nome"]
    def get_list_display(self, request):
        return ("studente", "corso")

admin.site.register(Partecipazione, PartAdmin)


class FirmaAdmin(admin.ModelAdmin):
    model = Firma
    search_fields = ["ora", "commento"]
    def get_list_display(self, request):
        return ("docente", "corso", "ora")

admin.site.register(Firma, FirmaAdmin)


class StatoAdmin(admin.ModelAdmin):
    model = StatoAllOra
    search_fields = ["studente", "firma"]
    def get_list_display(self, request):
        return ("studente", "stato")

admin.site.register(StatoAllOra, StatoAdmin)
