# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models


class Utente(models.Model):
    account = models.OneToOneField(
        "auth.User",
        on_delete=models.CASCADE,
        verbose_name="Account di sistema"
    )

    amministratore = models.BooleanField(
        verbose_name="Amministratore",
        default=False
    )

    docente = models.BooleanField(
        verbose_name="Docente",
        help_text="L'utente tiene uno o più corsi",
        default=False
    )

    coordinatore = models.BooleanField(
        verbose_name="Coordinatore di classe",
        help_text="L'utente è un docente coordinatore di classe",
        default=False
    )


    def __str__(self):
        return self.account.get_full_name() or self.account.username


    class Meta:
        verbose_name = "account utente"
        verbose_name_plural = "account utenti"
        ordering = ("account__last_name", "account__first_name")


class Corso(models.Model):
    nome = models.CharField(
        max_length=300,
        verbose_name="Nome"
    )

    relatore = models.CharField(
        max_length=500,
        verbose_name="Relatore/i"
    )

    docente = models.ForeignKey(
        Utente,
        on_delete=models.CASCADE,
        verbose_name="Account del docente"
    )

    numero_max_studenti_per_classe = models.BigIntegerField(
        verbose_name="Numero massimo di partecipanti per sessione"
    )

    descrizione = models.TextField(
        verbose_name="Descrizione"
    )

    ora_inizio = models.DateTimeField(
        verbose_name="Ora inizio",
        help_text="Data ed ora di inizio della lezione"
    )

    ora_fine = models.DateTimeField(
        verbose_name="Ora fine",
        help_text="Data ed ora di fine della lezione"
    )

    studenti = models.ManyToManyField(
        Utente,
        blank=True,
        related_name="studenti",
        verbose_name="Partecipanti"
    )


    def __str__(self):
        return "%s da %s a %s" % (self.nome, self.ora_inizio, self.ora_fine)


    class Meta:
        verbose_name = "corso"
        verbose_name_plural = "corsi"


class Partecipazione(models.Model):
    studente = models.ForeignKey(
        Utente,
        on_delete=models.CASCADE,
        verbose_name="Account studente"
    )

    corso = models.ForeignKey(
        Corso,
        on_delete=models.CASCADE,
        verbose_name="Corso"
    )

    livello_preferenza = models.BigIntegerField(
        verbose_name="Livello preferenza"
    )

    commenti = models.TextField(
        verbose_name="Note",
        null=True,
        blank=True
    )

    ultima_modifica = models.DateTimeField(auto_now=True)


    def __str__(self):
        return "%s a %s" % (self.studente, self.corso)


    class Meta:
        verbose_name = "partecipazione"
        verbose_name_plural = "partecipazioni"
        unique_together = ("studente", "corso")


class Firma(models.Model):
    docente = models.ForeignKey(
        Utente,
        on_delete=models.CASCADE,
        verbose_name="Account del docente"
    )

    corso = models.ForeignKey(
        Corso,
        on_delete=models.CASCADE,
        verbose_name="Corso"
    )

    ora = models.BigIntegerField(
        verbose_name="Ora di scuola",
        help_text="In riferimento all'orario scolastico, da 1 a 7"
    )

    commento = models.TextField(
        blank=True,
        null=True
    )

    def __str__(self):
        return "Firma di %s per il corso %s (%s° ora)" % (
            self.docente, self.corso, self.ora
        )


    class Meta:
        verbose_name = "firma"
        verbose_name_plural = "firme"
        ordering = ("ora", )


class StatoAllOra(models.Model):
    studente = models.ForeignKey(
        Utente,
        on_delete=models.CASCADE,
        verbose_name="Account studente"
    )

    firma = models.ForeignKey(
        Firma,
        on_delete=models.CASCADE,
        verbose_name="Firma del docente"
    )

    stati = [
        ("PL", "Presente a lezione"),
        ("PX", "Presente fuori aula"),
        ("R", "Ritardo"),
        ("RB", "Ritardo breve"),
        ("AL", "Assente a lezione"),
    ]

    stato = models.CharField(max_length=2, choices=stati, default="PL")

    commenti = models.TextField(
        verbose_name="Note",
        null=True,
        blank=True
    )

    def __str__(self):
        return "Stato di %s alla %s° ora del %s" % (
            self.studente,
            self.firma.ora,
            self.firma.corso.ora_inizio.strftime("%d/%m/%Y")
        )

    class Meta:
        verbose_name = "stato all'ora"
        verbose_name_plural = "stati alle ore"
        unique_together = ("studente", "firma")
