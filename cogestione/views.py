# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .models import *
from .decorators import docente, studente, coord
from . import buttons
import json
from datetime import datetime, date


@login_required
def corsi(request):
    studente = Utente.objects.get(account=request.user)
    # Reindirizza se non studente
    if studente.docente or studente.amministratore:
        return redirect("cogestione:docente-home")
    elif studente.coordinatore:
        return redirect("cogestione:coord-home")

    # Variabilizza elenco corsi
    corsi_o = Corso.objects.all()

    # Cerca partecipazioni confermate
    partecipazioni = Partecipazione.objects.filter(studente=studente)

    # Trova prima data utile corsi (da passare a fullcalendar)
    prima_data = corsi_o.order_by("ora_inizio")[0].ora_inizio.strftime("%Y-%m-%d")

    # Renderizza in lista per fullcalendar
    corsi = [
        [
            a.id,
            a.nome,
            a.ora_inizio.strftime("%Y-%m-%dT%H:%M"),
            a.ora_fine.strftime("%Y-%m-%dT%H:%M"),
            a.descrizione,
            a.studenti.count() >= a.numero_max_studenti_per_classe,
            a.studenti.filter(id=studente.id).exists(),
            a.relatore
        ] for a in corsi_o
    ]

    return render(
        request,
        "cogestione/corsi.html",
        {"prima_data": prima_data, "corsi": corsi}
    )


@studente
def iscrivi(request):
    # Risolvi corso
    try:
        corso = Corso.objects.get(id=request.POST["id"])
    except Exception as e:
        messages.danger(request, "Errore durante la ricerca del corso. Il server ha risposto: %s" % e)
        return redirect("cogestione:index")

    studente = Utente.objects.get(account=request.user)
    if corso.studenti.count() >= corso.numero_max_studenti_per_classe:
        messages.danger(request, "Questo corso ha raggiunto il numero massimo d'iscrizioni")
        return redirect("cogestione:index")

    if corso.studenti.filter(id=studente.id).exists():
        return redirect("cogestione:index")

    iscrizioni = Corso.objects.filter(
        studenti__id=studente.id,
        ora_inizio__gte=corso.ora_inizio,
        ora_inizio__lt=corso.ora_fine
    )
    if iscrizioni.exists():
        messages.danger(request, "Hai già un'altro corso nella stessa fascia oraria!")
        return redirect("cogestione:index")

    corso.studenti.add(studente)
    corso.save()

    return redirect("cogestione:index")


@studente
def cancella_iscrizione(request):
    # Risolvi corso
    try:
        corso = Corso.objects.get(id=request.POST["id"])
    except Exception as e:
        messages.danger(request, "Errore durante la ricerca del corso. Il server ha risposto: %s" % e)
        return redirect("cogestione:index")

    studente = Utente.objects.get(account=request.user)

    query = corso.studenti.filter(id=studente.id)
    if len(query) > 1:
        messages.danger(request, "Errore generale durante la cancellazione (QUERY_MORE_THAN_ONE_RESULT)")
        return redirect("cogestione:index")
    corso.studenti.remove(studente)
    corso.save()
    return redirect("cogestione:index")


##############################################################################
#                INIZIO PARTE DOCENTI                                        #
##############################################################################

@docente
def home_docente(request):
    return render(request, "cogestione/home_docente.html")


@docente
def corsi_docente(request):
    corsi = Corso.objects.filter(docente=Utente.objects.get(account=request.user))
    return render(request, "cogestione/miei_corsi.html", {"corsi": corsi})


@docente
def tutti_i_corsi_docente(request):
    corsi = Corso.objects.all()
    return render(request, "cogestione/tutti_i_corsi.html", {"corsi": corsi})


@docente
def corso_docente(request, corso):
    corso = Corso.objects.get(id=corso)
    giorno = corso.ora_inizio.strftime("%d/%m/%Y")
    return render(
        request,
        "cogestione/relatore_studenti.html",
        {"corso": corso, "giorno": giorno}
    )


@docente
def studenti_classe_ajax(request, corso):
    try:
        corso = Corso.objects.get(id=corso)
    except:
        return HttpResponse("COURSE ERROR")

    # Ricerca studenti
    giorno = corso.ora_inizio
    studenti = corso.studenti.all()

    all = []
    # Preparazione tabella
    for studente in studenti:
        this = ["", "", "", "", "", "", "", "", ""]
        this[0] = buttons.checkbox(studente)
        this[1] = "%s %s" % (studente.account.last_name, studente.account.first_name)
        # Cerca firme e renderizza
        stati = StatoAllOra.objects.filter(
            studente=studente,
            firma__corso__ora_inizio__date=giorno
        )
        for stato in stati:
            this[1+stato.firma.ora] = buttons.stato_alla_lezione(
                stato, corso, stato.firma.docente.account == request.user
            )

        all.append(this)

    return HttpResponse(json.dumps({"aaData": all}))


@docente
def firme_classe_ajax(request, corso):
    try:
        corso = Corso.objects.get(id=corso)
    except:
        return HttpResponse("COURSE ERROR")

    # Ricerca studenti
    giorno = corso.ora_inizio
    studenti = corso.studenti.all()

    stati = StatoAllOra.objects.filter(
        studente__in=studenti, firma__corso__ora_inizio__date=giorno
    ).values('firma').distinct().order_by("firma__ora")
    firme = [Firma.objects.get(id=a["firma"]) for a in stati]

    # Renderizza tabella
    all = []
    for firma in firme:
        this = ["", "", "", ""]
        u = firma.docente.account
        this[0] = "%s %s" % (u.last_name, u.first_name)
        this[1] = "%s°" % firma.ora
        this[2] = firma.corso.nome
        this[3] = buttons.elimina_firma(firma, request.user)
        all.append(this)

    return HttpResponse(json.dumps({"aaData": all}))


@docente
def cambio_stato(request, corso):
    try:
        stato = StatoAllOra.objects.get(id=request.POST.get("id"))
        if stato.firma.docente.account != request.user:
            return HttpResponse("Impossibile modificare lo stato: la firma non è stata apposta da voi!")
        stato.stato = request.POST["nuovostato"]
        stato.save()
        return HttpResponse("ok")
    except Exception as e:
        return HttpResponse("Errore: %s" % e)


@docente
def cancella_stato(request, corso):
    try:
        stato = StatoAllOra.objects.get(id=request.POST.get("id"))
        if stato.firma.docente.account != request.user:
            return HttpResponse("Impossibile modificare lo stato: la firma non è stata apposta da voi!")
        stato.delete()
        return HttpResponse("ok")
    except Exception as e:
        return HttpResponse("Errore: %s" % e)


@docente
def cancella_firma(request, corso):
    try:
        firma = Firma.objects.get(id=request.POST.get("firma"))
        if firma.docente.account != request.user:
            return HttpResponse("Impossibile modificare lo stato: la firma non è stata apposta da voi!")
        firma.delete()
        return HttpResponse("ok")
    except Exception as e:
        return HttpResponse("Errore: %s" % e)


@docente
def esegui_firma(request, corso):
    try:
        corso = Corso.objects.get(id=corso)
        ora = int(request.POST.get("ora_scol"))
        try:
            nuova_firma = Firma.objects.get(
                docente=Utente.objects.get(account=request.user),
                corso=corso,
                ora=ora
            )
        except:
            nuova_firma = Firma.objects.create(
                docente=Utente.objects.get(account=request.user),
                corso=corso,
                ora=ora
            )
        giorno = corso.ora_inizio
        studenti = corso.studenti.all()
        errori = ""
        for studente in studenti:
            # Cerco le firme già apposte per verificare che non ci siano conflitti
            try:
                StatoAllOra.objects.get(
                    studente=studente,
                    firma__corso__ora_inizio__date=giorno,
                    firma__ora=ora
                )
                errori += "Non è stato possibile salvare lo stato di %s: ora già firmata\n" % studente.account.get_full_name()
            except:
                stati = StatoAllOra.objects.filter(
                    studente=studente,
                    firma__corso__ora_inizio__date=giorno
                ).order_by('-firma__ora')
                if not stati:
                    pre_stato = "PL"
                else:
                    pre_stato = stati[0].stato
                if pre_stato in "RB":
                    pre_stato = "PL"
                StatoAllOra.objects.create(
                    studente=studente,
                    firma=nuova_firma,
                    stato=pre_stato
                )
    except Exception as e:
        return HttpResponse("Errore: %s" % e)
    return HttpResponse(errori if errori else "ok")


@docente
def esegui_firma_selettiva(request, corso):
    try:
        corso = Corso.objects.get(id=corso)
        id_studenti = request.POST.get("ids").split(",")
        ora = int(request.POST.get("ora_scol"))
        try:
            nuova_firma = Firma.objects.get(
                docente=Utente.objects.get(account=request.user),
                corso=corso,
                ora=ora
            )
        except:
            nuova_firma = Firma.objects.create(
                docente=Utente.objects.get(account=request.user),
                corso=corso,
                ora=ora
            )
        giorno = corso.ora_inizio
        studenti = [Utente.objects.get(id=int(a)) for a in id_studenti]
        errori = ""
        for studente in studenti:
            # Cerco le firme già apposte per verificare che non ci siano conflitti
            try:
                StatoAllOra.objects.get(
                    studente=studente,
                    firma__corso__ora_inizio__date=giorno,
                    firma__ora=ora
                )
                errori += "Non è stato possibile salvare lo stato di %s: ora già firmata\n" % studente.account.get_full_name()
            except:
                stati = StatoAllOra.objects.filter(
                    studente=studente,
                    firma__corso__ora_inizio__date=giorno
                ).order_by('-firma__ora')
                if not stati:
                    pre_stato = "PL"
                else:
                    pre_stato = stati[0].stato
                if pre_stato in "RB":
                    pre_stato = "PL"
                StatoAllOra.objects.create(
                    studente=studente,
                    firma=nuova_firma,
                    stato=pre_stato
                )
    except Exception as e:
        return HttpResponse("Errore: %s" % e)
    return HttpResponse(errori if errori else "ok")


##############################################################################
#             INIZIO PARTE DOCENTI COORDINATORI DI CLASSE                    #
##############################################################################

@coord
def coord_index(request):
    giorno = datetime.now()
    if request.GET.get("date"):
        giorno = datetime.strptime(request.GET["date"], "%Y-%m-%d")
    return render(
        request,
        "cogestione/coord_index.html",
        {'day': request.GET.get("date", ""), "date": giorno}
    )


@coord
def coord_studenti_ajax(request):
    giorno = datetime.now()
    if request.GET.get("giorno"):
        giorno = datetime.strptime(request.GET["giorno"], "%Y-%m-%d")
    studenti = Utente.objects.filter(
        amministratore=False,
        docente=False,
        coordinatore=False
    ).order_by('account__last_name')

    all = []
    # Preparazione tabella
    for studente in studenti:
        this = ["", "", "", "", "", "", "", ""]
        this[0] = "%s %s" % (studente.account.last_name, studente.account.first_name)
        # Cerca firme e renderizza
        stati = StatoAllOra.objects.filter(
            studente=studente,
            firma__corso__ora_inizio__date=giorno
        )
        for stato in stati:
            this[stato.firma.ora] = buttons.stato_alla_lezione_coord(stato)

        all.append(this)

    return HttpResponse(json.dumps({"aaData": all}))


@coord
def coord_firme_ajax(request):
    giorno = datetime.now()
    if request.GET.get("giorno"):
        giorno = datetime.strptime(request.GET["giorno"], "%Y-%m-%d")
    studenti = Utente.objects.filter(
        amministratore=False,
        docente=False,
        coordinatore=False
    ).order_by('account__last_name')

    stati = StatoAllOra.objects.filter(
        studente__in=studenti, firma__corso__ora_inizio__date=giorno
    ).values('firma').distinct().order_by("firma__ora")
    firme = [Firma.objects.get(id=a["firma"]) for a in stati]

    # Renderizza tabella
    all = []
    for firma in firme:
        this = ["", "", "", ""]
        u = firma.docente.account
        this[0] = "%s %s" % (u.last_name, u.first_name)
        this[1] = "%s°" % firma.ora
        this[2] = firma.corso.nome
        this[3] = buttons.elimina_firma(firma, request.user)
        all.append(this)

    return HttpResponse(json.dumps({"aaData": all}))
