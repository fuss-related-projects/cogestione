from django.contrib.auth.decorators import login_required
from .models import Utente


def docente(view, *args, **kwargs):
    @login_required
    def do_check(request, *args, **kwargs):
        try:
            a = Utente.objects.get(account=request.user)
            if not (a.docente or a.amministratore):
                raise Exception("Non sei un docente")
        except:
            messages.danger(
                request,
                "Non sei un docente!"
            )
            return redirect("index")
        return view(request, *args, **kwargs)
    return do_check


def studente(view, *args, **kwargs):
    @login_required
    def do_check(request, *args, **kwargs):
        try:
            a = Utente.objects.get(account=request.user)
            if a.docente or a.amministratore:
                raise Exception("Non sei uno studente")
        except:
            messages.danger(
                request,
                "Non sei uno studente!"
            )
            return redirect("index")
        return view(request, *args, **kwargs)
    return do_check


def coord(view, *args, **kwargs):
    @login_required
    def do_check(request, *args, **kwargs):
        try:
            a = Utente.objects.get(account=request.user)
            if not a.coordinatore:
                raise Exception("Non sei un coordinatore")
        except:
            messages.danger(
                request,
                "Non sei un coordinatore!"
            )
            return redirect("index")
        return view(request, *args, **kwargs)
    return do_check
